#! python3
import random

players = []
enemies = []
combat = []


def add_players():
    name = input('Name:  ')
    player = [name, 0]
    players.append(player)
    action = input('Want to add another player? (Y,n):  ')
    if action == 'n':
        return
    else:
        add_players()


def add_enemies():
    type = input('type: ')
    amount = input('How many?  ')

    for i in range(1, int(amount) + 1):
        enemy = [type + '-' + str(i), 0]
        enemies.append(enemy)

    action = input('Want to add another enemy? (Y,n):  ')
    if action == 'n':
        return
    else:
        add_enemies()


def set_initiative():
    for player in players:
        player[1] = int(input('Set initiative for player: ' + player[0] + ':  '))

    action = input('Use auto initiative for enemies? (Y,n):  ')
    if action == 'n':
        for enemy in enemies:
            enemy[1] = int(input('Set initiative for enemy: ' + enemy[0] + ':  '))
    else:
        for enemy in enemies:
            enemy[1] = random.randint(1, 20)

    combat.extend(players + enemies)
    combat.sort(key=lambda e: e[1], reverse=True)


def run_combat():
    combat_active = True
    i = 0
    while combat_active:
        active = combat[i]
        input('It is ' + active[0] + '\'s turn')
        i = (i+1) % (len(combat) - 1)


def setup():
    print('Add players:')
    add_players()

    print('\nAdd Enemies:')
    add_enemies()

    input('Press enter when ready for combat')
    set_initiative()

    run_combat()


if __name__ == '__main__':
    setup()
